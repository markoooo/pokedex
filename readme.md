# Pokémon Encyclopedia - Pokédex

Mini Pokédex attempt in React & Redux

### Installation

Some of the standard Node tools are required: [Node](https://nodejs.org/en/), [Yarn](https://yarnpkg.com/en/) or [NPM](https://www.npmjs.com/get-npm) and[Gulp](https://gulpjs.com/) in this case.

Dependencies installation:
```
yarn install
```
or
```
npm install
```

### Running the app

You can run the gulp `dev` task to start the app:
```
gulp dev
```