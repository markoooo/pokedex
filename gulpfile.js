const
    autoprefixer = require('gulp-autoprefixer'),
    babelify = require('babelify'),
    browserify = require('browserify'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect'),
    del = require('del'),
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    opn = require('opn'),
    sass = require('gulp-sass'),
    seq = require('run-sequence'),
    source = require('vinyl-source-stream');

const buildDir = './dest',
    files = {
        js: {
            dest: 'main.js',
            libs: [
                './node_modules/pokeapi-js-wrapper/dist/index.js'
            ],
            main: './src/js/main.js',
            watch: [
                './src/js/**/*.js',
                './src/js/**/*.jsx'
            ],
        },
        scss: {
            main: './src/scss/main.scss',
            watch: [
                './src/scss/**/*.scss'
            ]
        },
        html: ['./src/**/*.html'],
        fonts: ['./src/fonts/**/*'],
        images: ['./src/images/**/*']
    };

// Main tasks

gulp.task('build:jsx', function () {
    browserify({
            entries: './src/js/app.jsx',
            debug: true,
            extensions: ['.js', '.jsx'],
            paths: ['./src/js/']
        })
        .transform(babelify, {
            presets: ['es2015', 'react']
        })
        .bundle()
        .on('error', gutil.log)
        .pipe(source('main.js'))
        .pipe(gulp.dest(buildDir + '/js'))
        .pipe(connect.reload());
});

gulp.task('build:libs', function () {
    return gulp.src(files.js.libs)
        .pipe(concat('libs.js'))
        .pipe(gulp.dest(buildDir + '/js'));
});

gulp.task('clean', function () {
    return del(buildDir, {
        force: true
    });
});

gulp.task('connect', function () {
    connect.server({
        name: 'Pokedex Preview',
        root: [buildDir],
        port: 7200,
        livereload: true
    });
    setTimeout(function () {
        opn('http://localhost:7200');
    }, 720);
});

gulp.task('fonts', function () {
    return gulp.src(files.fonts)
        .pipe(gulp.dest(buildDir + '/fonts'));
});

gulp.task('images', function () {
    return gulp.src(files.images)
        .pipe(gulp.dest(buildDir + '/images'));
});

gulp.task('html', function () {
    return gulp.src(files.html)
        .pipe(gulp.dest(buildDir))
        .pipe(connect.reload());
});

gulp.task('scss', function () {
    return gulp.src(files.scss.main)
        .pipe(sass().on('error', gutil.log))
        .pipe(autoprefixer('last 6 versions', '> 3%'))
        .pipe(concat('style.css'))
        .pipe(gulp.dest(buildDir + '/css'))
        .pipe(connect.reload());
});

gulp.task('watch', function () {
    gulp.watch(files.js.watch, {
        debounceDelay: 500
    }, ['build:jsx']);
    gulp.watch(files.html, {
        debounceDelay: 500
    }, ['html']);
    gulp.watch(files.scss.watch, {
        debounceDelay: 500
    }, ['scss']);
    gulp.watch(files.images, {
        debounceDelay: 500
    }, ['images']);
});

// Compound tasks

gulp.task('build', function () {
    seq('clean', ['build:jsx', 'build:libs', 'html', 'scss', 'fonts', 'images']);
});

gulp.task('dev', seq('build', 'watch', 'connect'));