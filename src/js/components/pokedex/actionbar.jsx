import React from 'react';
import {connect} from 'react-redux';

import {changeScreen} from '../../actions/screenActions';

class ActionBar extends React.Component {
    constructor(props) {
        super(props);

        this.changeToList = this.changeToList.bind(this);
        this.changeToFavourites = this.changeToFavourites.bind(this);
    }
    render() {
        return (
            <div className="pokedex-frame-actionbar">
                <div className="pokedex-frame-actionbar__actions btn-group">
                    <button onClick={this.changeToList} className="pokedex-frame-actionbar__action btn btn--round btn--sm btn--icon" title="All Pokémon">
                        <i className="icon-list"></i>
                    </button>
                    <button onClick={this.changeToFavourites} className="pokedex-frame-actionbar__action btn btn--round btn--sm btn--icon" title="My Pokémon">
                        <i className="icon-heart"></i>
                    </button>
                </div>
                <div className="pokedex-frame-actionbar__speaker" title="This is the speaker.">
                    <div className="pokedex-speaker__hole"></div>
                    <div className="pokedex-speaker__hole"></div>
                    <div className="pokedex-speaker__hole"></div>
                    <div className="pokedex-speaker__hole"></div>
                </div>
            </div>
        );
    }

    changeToList() {
        this.props.handleScreenChange('list');
    }
    changeToFavourites() {
        this.props.handleScreenChange('favourites');
    }
};

const mapDispatchToProps = function(dispatch) {
    return {
        handleScreenChange: (screen) => dispatch(changeScreen(screen))
    };
}

export default connect(null, mapDispatchToProps)(ActionBar);
