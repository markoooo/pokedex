import React from 'react';
import {connect} from 'react-redux';

import {changeScreen} from '../../actions/screenActions';

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.changeToMain = this.changeToMain.bind(this);
    }

    render() {
        return (
            <div className="pokedex-header">
                <div className="pokedex-header__lights">
                    <div onClick={this.changeToMain} className="light-main" title="This does nothing."> {/* I lied, it does do something. Do be do be doo. */}
                        <div className="light-main__bulb"></div>
                    </div>
                    <div className="light-small light--red"></div>
                    <div className="light-small light--yellow"></div>
                    <div className="light-small light--green"></div>
                </div>
                <div className="pokedex-header__title">
                    <h1 title="Expandable Pokédex">{this.props.title}</h1>
                </div>
            </div>
        );
    }

    changeToMain() {
        this.props.handleScreenChange('main');
    }
}


const mapDispatchToProps = function(dispatch) {
    return {
        handleScreenChange: (screen) => dispatch(changeScreen(screen))
    };
}

export default connect(null, mapDispatchToProps)(Header);