import React from 'react';

export default class Loading extends React.Component {
    render() {
        return (
            <div className="loading">
                <div className="loader-dots">
                    <span>.</span>
                    <span>.</span>
                    <span>.</span>
                </div>
            </div>
        );
    }
};
