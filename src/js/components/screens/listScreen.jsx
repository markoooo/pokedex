
import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';

import Loading from '../loading';
import ResultItem from '../pokemon/resultItem';

import {changeScreen} from '../../actions/screenActions';
import {fetchFavourites} from '../../actions/favouritesActions';
import {fetchPokemons} from '../../actions/pokemonsActions';

class ListScreen extends React.Component {
    constructor(props) {
        super(props);

        this.openPokemon = this.openPokemon.bind(this);
        this.previousPage = this.previousPage.bind(this);
        this.nextPage = this.nextPage.bind(this);
        this.reloadData = this.reloadData.bind(this);
    }

    componentWillMount() {
        console.log('Showing list');
        this.reloadData();
    }

    render() {
        if(this.props.type === 'favourites' && this.props.favouritesState.fetching || this.props.pokemonState.fetching) {
            return (
                <div className="pokedex-screen pokedex-screen-list">
                    <Loading />
                </div>
            );
        }
        return (
            <div className="pokedex-screen pokedex-screen-list">
                <div className="text-center">{this.props.title}</div>
                <div className="pokedex-result-list">
                    {this.renderItems()}
                </div>
                {this.props.type !== 'favourites' && this.renderPagination()}
            </div>
        );
    }

    // Events
    openPokemon(id) {
        this.props.dispatch(changeScreen('details', id));
    }

    previousPage() {
        this.props.dispatch(fetchPokemons(this.props.pokemonState.previousOffset));
    }

    nextPage() {
        this.props.dispatch(fetchPokemons(this.props.pokemonState.nextOffset));
    }

    reloadData() {
        if(this.props.type === 'favourites') {
            this.props.dispatch(fetchFavourites());
        }
        else {
            this.props.dispatch(fetchPokemons(this.props.pokemonState.currentOffset));
        }
    }

    // Custom
    renderItems() {
        let state = (this.props.type === 'favourites' ? this.props.favouritesState : this.props.pokemonState);
        if(state.error) {
            return (
                <div className="pokedex-result-list-error">
                    <div>Error: {state.error.message}</div>
                    <div><button onClick={this.reloadData} className="btn btn--screen">Reload</button></div>
                </div>
            );
        }
        if(state.fetching) {
            return (
                <div>Loading...</div>
            );
        }
        if(!state.fetched) {
            this.reloadData();
            return;
        }
        return _.map(state.pokemons, (pokemon) => {
            return <ResultItem onClick={this.openPokemon} key={pokemon.id} id={pokemon.id} name={pokemon.name} image={pokemon.sprite} />;
        });
    }

    renderPagination() {
        let state = this.props.pokemonState;
        if(state.error) {
            return;
        }
        return (
            <div className="pokedex-result-pagination">
                {state.previousOffset !== null && <button onClick={this.previousPage} className="pokedex-result-pagination-button pokedex-result-pagination-button--prev"></button> }
                {state.nextOffset !== null && <button onClick={this.nextPage} className="pokedex-result-pagination-button pokedex-result-pagination-button--next"></button> }
            </div>
        );
    }
};


const mapStateToProps = function(store) {
    return {
        pokemonState: store.pokemons,
        favouritesState: store.favourites
    };
}

export default connect(mapStateToProps)(ListScreen);