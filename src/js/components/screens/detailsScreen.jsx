import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';

import {fetchPokemonInfo} from '../../actions/pokemonActions';
import {fetchFavourites, addFavourite, removeFavourite} from '../../actions/favouritesActions';

import Loading from '../loading.jsx';
import DetailsInfo from '../details/detailsInfo.jsx';
import DetailsMoves from '../details/detailsMoves.jsx';
import DetailsStats from '../details/detailsStats.jsx';

class DetailsScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            supage: 'info'
        };

        this.changeToInfo = this.changeToInfo.bind(this);
        this.changeToStats = this.changeToStats.bind(this);
        this.changeToMoves = this.changeToMoves.bind(this);
        this.toggleFavorite = this.toggleFavorite.bind(this);
    }

    componentWillMount() {
        this.props.dispatch(fetchPokemonInfo(this.props.id));
        this.props.dispatch(fetchFavourites());
        this.setState({subpage: 'info' });
    }

    render() {
        if(!this.props.selectedPokemonState.pokemon.id){
            return (
                <div className="pokedex-screen pokedex-screen-info">
                    <Loading />
                </div>
            );
        }

        let pokemon = this.props.selectedPokemonState.pokemon,
            properName = _.startCase(pokemon.name);
        let image = (pokemon.sprites ? pokemon.sprites.front_default : null);

        return (
            <div className="pokedex-screen pokedex-screen-info">
                <div className="pokedex-screen-name">{properName}</div>
                {this.renderSubpage()}
                <div className="pokedex-screen-subnav">
                    <button onClick={this.changeToInfo} className={'btn btn--screen ' + (this.state.subpage === 'info' ? 'active' : '')}>Info</button>
                    <button onClick={this.changeToStats} className={'btn btn--screen ' + (this.state.subpage === 'stats' ? 'active' : '')}>Stats</button>
                    <button onClick={this.changeToMoves} className={'btn btn--screen ' + (this.state.subpage === 'moves' ? 'active' : '')}>Moves</button>
                    <button onClick={this.toggleFavorite} className={'btn btn--screen'}><i className={this.isFavourite() ? 'icon-heart' : 'icon-heart-empty'}></i></button>
                </div>
            </div>
        );
    }

    renderSubpage() {

        switch(this.state.subpage) {
            case 'moves':
                return <DetailsMoves moves={this.props.selectedPokemonState.pokemon.moves} />;
            case 'stats':
                return <DetailsStats stats={this.props.selectedPokemonState.pokemon.stats} />;
            case 'info':
            default:
                return <DetailsInfo pokemon={this.props.selectedPokemonState.pokemon} />;
        }
    }

    changeToInfo() {
        this.setState({ subpage: 'info' });
    }

    changeToStats() {
        this.setState({ subpage: 'stats' });
    }

    changeToMoves() {
        this.setState({ subpage: 'moves' });
    }

    toggleFavorite() {
        let pokemon = {
            id: this.props.selectedPokemonState.pokemon.id,
            sprite: this.props.selectedPokemonState.pokemon.sprites.front_default,
            name: this.props.selectedPokemonState.pokemon.name
        };
        if(this.isFavourite()) {
            this.removePokemonFromFavourites(pokemon);
        }
        else {
            this.addPokemonToFavourites(pokemon);
        }
    }

    addPokemonToFavourites(pokemon) {
        this.props.dispatch(addFavourite(pokemon));
        this.props.dispatch(fetchFavourites());
    }

    removePokemonFromFavourites(pokemon) {
        this.props.dispatch(removeFavourite(pokemon));
        this.props.dispatch(fetchFavourites());
    }

    isFavourite() {
        let isFavourite = false,
            favourites = this.props.favouritesState.pokemons,
            selectedPokemon = this.props.selectedPokemonState.pokemon;

        _.forEach(favourites, (favourite) => {
            if(favourite.id == selectedPokemon.id) {
                isFavourite = true;
                return false;
            }
        });

        return isFavourite;
    }
};

const mapStateToProps = function(state) {

    return {
        selectedPokemonState: state.pokemon,
        favouritesState: state.favourites
    };
}

export default connect(mapStateToProps)(DetailsScreen);