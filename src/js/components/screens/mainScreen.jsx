
import React from 'react';

export default class MainScreen extends React.Component {
    render() {
        return (
            <div className="pokedex-screen">
                <p className="text-center">Welcome to the world of Pokémon!</p>
                <p className="text-center">
                    <img src="images/oak.png" alt="Professor Oak" />
                </p>
            </div>
        );
    }
}