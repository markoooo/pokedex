import React from 'react';

import Header from './pokedex/header.jsx';
import ActionBar from './pokedex/actionbar.jsx';
import Screen from './screen.jsx';

export default class Pokedex extends React.Component {
    render() {
        return (
            <div className="pokedex-wrapper">
                <div className="pokedex">
                    <Header title="Pokédex" />
                    <div className="pokedex-inlay">
                        <div className="pokedex-frame">
                            <Screen />
                            <ActionBar />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}