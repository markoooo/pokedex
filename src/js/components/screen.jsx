import React from 'react';
import { connect } from 'react-redux';

import DetailsScreen from './screens/detailsScreen.jsx';
import ListScreen from './screens/listScreen.jsx';
import MainScreen from './screens/mainScreen.jsx';

class Screen extends React.Component {
    render() {
        var activeScreen;
        console.log('Activating screen', this.props.screenState.screen, this.props.screenState.id)
        switch(this.props.screenState.screen) {
            case 'details':
                activeScreen = <DetailsScreen id={this.props.screenState.id} />;
                break;
            case 'favourites':
                activeScreen = <ListScreen type="favourites" title="My Favourites" />;
                break;
            case 'list':
                activeScreen = <ListScreen type="list" title="Pokémon List" />;
                break;
            case 'main':
            default:
                activeScreen = <MainScreen />;
        }

        return activeScreen;
    }
}

const mapStateToProps = function(store) {
    return {
        screenState: store.screen
    };
}

export default connect(mapStateToProps)(Screen);