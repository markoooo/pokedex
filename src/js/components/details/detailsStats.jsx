import React from 'react';
import _ from 'lodash';

import DetailsStat from './stats/detailsStat.jsx';

export default class DetailsStats extends React.Component {
    render() {
        return (
            <div className="pokedex-screen-section pokedex-screen-section--stats">
                {this.buildStats()}
            </div>
        );
    }

    buildStats() {
        return _.map(this.props.stats, (stat, index) => {
            return <DetailsStat key={index} stat={stat} />
        });
    }
};
