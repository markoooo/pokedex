import React from 'react';
import _ from 'lodash';

export default class DetailsType extends React.Component {
    render() {
        var properType = _.startCase(this.props.type);
        return (
            <div className={'pokedex-info-type pokedex-info-type--' + this.props.type}>
                {properType}
            </div>
        );
    }
};
