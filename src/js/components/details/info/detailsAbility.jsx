import React from 'react';
import _ from 'lodash';

export default class DetailsAbility extends React.Component {
    render() {
        var properAbility = _.startCase(this.props.ability.ability.name);
        return (
            <li className="pokedex-info-ability">
                {properAbility}
            </li>
        );
    }
};
