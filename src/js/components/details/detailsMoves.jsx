import React from 'react';
import _ from 'lodash';

import DetailsMove from './moves/detailsMove.jsx';

export default class DetailsMoves extends React.Component {
    render() {
        return (
            <div className="pokedex-screen-section pokedex-screen-section--moves">
                {this.buildMoves()}
            </div>
        );
    }

    buildMoves() {
        let moves = this.props.moves,
            learnableMoves = _.reduce(moves, (filteredMoves, move, index) => {
                let learnableAt = false;
                _.forEach(move.version_group_details, (moveDeatils) => {
                    if(moveDeatils.move_learn_method.name === 'level-up') {
                        learnableAt = moveDeatils.level_learned_at;
                        return false;
                    }
                });
                if(learnableAt) {
                    filteredMoves.push({
                        name: _.startCase(move.move.name),
                        learnedAt: learnableAt
                    });
                }
                return filteredMoves;
            }, []);
        learnableMoves = learnableMoves.sort((a, b) => {
            return a.learnedAt > b.learnedAt;
        });
        return _.map(learnableMoves, (move, index) => {
            return <DetailsMove key={index} move={move} />
        });
    }
};
