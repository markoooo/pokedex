import React from 'react';
import _ from 'lodash';

export default class DetailsMove extends React.Component {
    render() {
        return (
            <div className="pokedex-info-move">
                <div className="pokedex-info-move__name">{this.props.move.name}</div>
                <div className="pokedex-info-move__value">(Lv. {this.props.move.learnedAt})</div>
            </div>
        );
    }
};
