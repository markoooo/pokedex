import React from 'react';
import _ from 'lodash';

export default class DetailsAbility extends React.Component {
    render() {
        var properStat = _.startCase(this.props.stat.stat.name),
            baseStat = this.props.stat.base_stat;
        return (
            <div className="pokedex-info-stat">
                <div className="pokedex-info-stat__name">{properStat}</div>
                <div className="pokedex-info-stat__value">
                    <div className="pokedex-info-stat__value-bar" style={{ width: Math.round(baseStat / 255 * 100) + 'px'}}></div>
                    <span>{baseStat}</span>
                </div>
            </div>
        );
    }
};
