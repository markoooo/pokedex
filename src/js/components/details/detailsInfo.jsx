import React from 'react';
import _ from 'lodash';

import DetailsAbility from './info/detailsAbility.jsx';
import DetailsType from './info/detailsType.jsx';

export default class DetailsInfo extends React.Component {
    render() {
        let pokemon = this.props.pokemon,
            properName = _.startCase(pokemon.name);
        let image = (pokemon.sprites ? pokemon.sprites.front_default : null);

        return (
            <div className="pokedex-screen-section pokedex-screen-section--info">
                {image && <div className="text-center"><img className="result-item__image" src={image} alt={properName} /></div>}
                <div className="pokedex-info-lines">
                    <div className="pokedex-info-line">
                        <div className="pokedex-info-line__property">Type:</div>
                        <div className="pokedex-info-line__value">
                            {this.buildTypes(pokemon.types)}
                        </div>
                    </div>
                    <div className="pokedex-info-line">
                        <div className="pokedex-info-line__property">Height:</div>
                        <div className="pokedex-info-line__value">
                            {+(pokemon.height / 10).toFixed(1)} m
                        </div>
                    </div>
                    <div className="pokedex-info-line">
                        <div className="pokedex-info-line__property">Weight:</div>
                        <div className="pokedex-info-line__value">
                            {+(pokemon.weight / 10).toFixed(1)} kg
                        </div>
                    </div>
                    <div className="pokedex-info-line pokedex-info-line--block">
                        <div className="pokedex-info-line__property">Abilities:</div>
                        <div className="pokedex-info-line__value">
                            <ul>
                                {this.buildAbilities(pokemon.abilities)}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    buildAbilities(abilities) {
        return _.map(abilities, (ability, index) => {
            return <DetailsAbility ability={ability} key={index} />
        });
    }

    buildTypes(types) {
        return _.map(types, (item, index) => {
            return <DetailsType type={item.type.name} key={index} />
        });
    }
};
