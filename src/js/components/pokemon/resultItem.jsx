
import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

//import {showPokemon} from '../../actions/pokemonActions';

export default class ResultItem extends React.Component {
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
    }

    render() {
        var properName = _.startCase(this.props.name);
        return (
            <div onClick={this.onClick} className="pokedex-result-list-item">
                <img className="result-item__image" src={this.props.image} alt={properName} />
                <div className="result-item__name">{properName}</div>
            </div>
        );
    }

    onClick() {
        this.props.onClick(this.props.id);
    }
};

//export default connect(mapStateToProps)(ListScreen);