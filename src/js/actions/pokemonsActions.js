const Pokedex = require('pokeapi-js-wrapper');
const P = new Pokedex.Pokedex({
    protocol: 'https',
    versionPath: '/api/v2/',
    cache: true,
    timeout: 20 * 1000
  });


export function fetchPokemons(offset) {
    console.log('Fetching pokemon from', offset);
    return function (dispatch) {
        let interval = {
            limit: 10,
            offset: offset || 0
        }
        P.getPokemonsList(interval)
            .then((response) => {
                response = parseResponse(response);
                dispatch({
                    type: 'FETCH_POKEMONS_FULFILLED',
                    response,
                    currentOffset: offset
                });
            })
            .catch((error) => {
                dispatch({
                    type: 'FETCH_POKEMONS_REJECTED',
                    error,
                    currentOffset: offset
                });
            });
    }
}

function parseResponse(response) {
    response.results = parsePokemonsResults(response.results);
    response.previousOffset = extractOffsetFromUrl(response.previous);
    response.nextOffset = extractOffsetFromUrl(response.next);

    return response;
}

function parsePokemonsResults(results) {
    let updatedResults = [];

    results.map(function(pokemon) {
        pokemon.id = extractIdFromUrl(pokemon.url);
        pokemon.sprite = buildSpriteUrl(pokemon.id);
        updatedResults.push(pokemon);
    });

    return updatedResults;
}

function extractIdFromUrl(url) {
    // https://pokeapi.co/api/v2/pokemon/[id]/
    let regex = /https:\/\/pokeapi\.co\/api\/v2\/pokemon\/(\d+)\/?/,
        tokens = url.match(regex),
        id = +(tokens[1]);

    return id;
}

function buildSpriteUrl(id) {
    // https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/[id].png
    let url = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
    return url;
}

function extractOffsetFromUrl(url) {
    if(!url) {
        return null;
    }
    let regex = /offset=(\d+)/,
        tokens = url.match(regex),
        offset = tokens && tokens.length > 0 ? +(tokens[1]) : 0;
    return offset;
}