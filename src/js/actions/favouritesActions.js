import depot from 'depotjs';
import _ from 'lodash';

const savedFavourites = depot('pokemon_favourites');

export function fetchFavourites() {
    console.log('Fetching favourited pokemon');
    return function (dispatch) {
        try {
            let favourites = savedFavourites.all();
            dispatch({
                type: 'FETCH_FAVOURITES_FULFILLED',
                favourites
            });
        }
        catch (error) {
            dispatch({
                type: 'FETCH_FAVOURITES_REJECTED',
                error
            });
        }
    }
}

export function addFavourite(pokemon) {
    console.log('Adding a pokemon to favourites', pokemon);
    return function (dispatch) {
        try {
            let alreadyFavourited = savedFavourites.find({id: pokemon.id});
            if (alreadyFavourited && alreadyFavourited.length > 0) {
                let error = new Error('This pokemon is already on your favourites list.');
                let favourites = savedFavourites.all();
                dispatch({
                    type: 'FETCH_FAVOURITES',
                    favourites
                });
            }
            else {
                savedFavourites.save(pokemon);
                let favourites = savedFavourites.all();
                dispatch({
                    type: 'ADD_FAVOURITES_FULFILLED',
                    favourites
                });
            }
        }
        catch (error) {
            dispatch({
                type: 'FETCH_FAVOURITES',
                favourites
            });
        }
    }
}

export function removeFavourite(pokemon) {
    console.log('Removing a pokemon from favourites', pokemon);
    return function (dispatch) {
        try {
            let favourite = savedFavourites.find({id: pokemon.id});
            if (!favourite || favourite.length === 0) {
                let error = new Error('This pokemon is not on your favourites list.');
                let favourites = savedFavourites.all();
                dispatch({
                    type: 'FETCH_FAVOURITES',
                    favourites
                });
            }
            else {
                savedFavourites.destroy(favourite[0]._id);
                let favourites = savedFavourites.all();
                dispatch({
                    type: 'REMOVE_FAVOURITES_FULFILLED',
                    favourites
                });
            }
        }
        catch (error) {
            dispatch({
                type: 'FETCH_FAVOURITES',
                favourites
            });
        }
    }
}