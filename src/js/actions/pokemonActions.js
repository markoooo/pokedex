const Pokedex = require('pokeapi-js-wrapper');
const P = new Pokedex.Pokedex({
    protocol: 'https',
    versionPath: '/api/v2/',
    cache: true,
    timeout: 20 * 1000
});

export function fetchPokemonInfo(id) {
    return function (dispatch) {
        P.getPokemonByName(id)
            .then((response) => {
                dispatch({
                    type: 'FETCH_POKEMON_FULFILLED',
                    response
                });
            })
            .catch((error) => {
                dispatch({
                    type: 'FETCH_POKEMON_REJECTED',
                    error
                });
            });
    }
}