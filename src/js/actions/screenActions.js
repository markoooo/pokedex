export function changeScreen(screen, id) {
    console.log('Dispatching new screen', screen, id);
    return {
        type: 'CHANGE_SCREEN',
        screen,
        id
    };
};