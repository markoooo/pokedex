import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";

import Pokedex from './components/pokedex.jsx';

import store from './store';

class App extends React.Component {
    componentDidMount() {
        window.setTimeout(() => {
            document.getElementById('app').classList.add('loaded');
        }, 2400);
    }
    render() {
        return (
            <div className="wrapper">
                <Pokedex />
            </div>
        );
    }
}

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);