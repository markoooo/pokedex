import {combineReducers} from 'redux';
import favourites from './favouritesReducer';
import pokemon from './pokemonReducer';
import pokemons from './pokemonsReducer';
import screen from './screenReducer';

export default combineReducers({
    favourites,
    pokemon,
    pokemons,
    screen
});