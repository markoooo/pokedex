export default function reducer(state = {
    id: null,
    screen: 'main'
}, action) {
    console.log('Reducing', action)
    switch (action.type) {
        case 'CHANGE_SCREEN':
            {
                return Object.assign({}, state, {
                    screen: action.screen,
                    id: action.id
                });
            }
    }
    return state;
};