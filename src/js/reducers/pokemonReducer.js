export default function reducer(state = {
    error: null,
    fetched: false,
    fetching: false,
    pokemon: {},
}, action) {
    switch (action.type) {
        case 'FETCH_POKEMON':
            {
                return Object.assign({}, state, {
                    fetching: true
                });
            }
        case 'FETCH_POKEMON_FULFILLED':
            {
                return Object.assign({}, state, {
                    fetched: true,
                    fetching: false,
                    pokemon: action.response
                });
            }
        case 'FETCH_POKEMON_REJECTED':
            {
                return Object.assign({}, state, {
                    error: action.error,
                    fetching: false
                });
            }
    }
    return state;
};