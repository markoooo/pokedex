export default function reducer(state = {
    count: 0,
    currentOffset: 0,
    error: null,
    fetched: false,
    fetching: false,
    next: null,
    nextOffset: null,
    pokemons: [],
    previous: null,
    previousOffset: null
}, action) {
    switch (action.type) {
        case 'FETCH_POKEMONS':
            {
                return Object.assign({}, state, {
                    fetching: true
                });
            }
        case 'FETCH_POKEMONS_FULFILLED':
            {
                return Object.assign({}, state, {
                    count: action.response.count,
                    currentOffset: action.currentOffset,
                    fetched: true,
                    fetching: false,
                    next: action.response.next,
                    nextOffset: action.response.nextOffset,
                    pokemons: action.response.results,
                    previous: action.response.previous,
                    previousOffset: action.response.previousOffset,
                });
            }
        case 'FETCH_POKEMONS_REJECTED':
            {
                return Object.assign({}, state, {
                    error: action.error,
                    fetching: false,
                    currentOffset: action.currentOffset
                });
            }
    }
    return state;
};