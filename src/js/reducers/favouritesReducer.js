export default function reducer(state = {
    error: null,
    fetched: false,
    fetching: false,
    pokemons: []
}, action) {
    switch (action.type) {
        case 'FETCH_FAVOURITES':
            {
                return Object.assign({}, state, {
                    fetching: true
                });
            }
        case 'FETCH_FAVOURITES_FULFILLED':
            {
                return Object.assign({}, state, {
                    fetched: true,
                    fetching: false,
                    pokemons: action.favourites
                });
            }
        case 'FETCH_FAVOURITES_REJECTED':
            {
                return Object.assign({}, state, {
                    error: action.error,
                    fetching: false
                });
            }
        case 'ADD_FAVOURITE':
            {
                return Object.assign({}, state, {
                    fetching: true
                });
            }
        case 'ADD_FAVOURITE_FULFILLED':
            {
                return Object.assign({}, state, {
                    fetched: true,
                    fetching: false,
                    pokemons: action.favourites
                });
            }
        case 'ADD_FAVOURITES_REJECTED':
            {
                return Object.assign({}, state, {
                    error: action.error,
                    fetching: false
                });
            }
        case 'REMOVE_FAVOURITE':
            {
                return Object.assign({}, state, {
                    fetching: true
                });
            }
        case 'REMOVE_FAVOURITE_FULFILLED':
            {
                return Object.assign({}, state, {
                    fetched: true,
                    fetching: false,
                    pokemons: action.favourites
                });
            }
        case 'REMOVE_FAVOURITES_REJECTED':
            {
                return Object.assign({}, state, {
                    error: action.error,
                    fetching: false
                });
            }
    }
    return state;
};